package com.project.interactivealarm;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.project.interactivealarm.pedometer.SimpleStepDetector;
import com.project.interactivealarm.pedometer.StepListener;

public class PedometerAlarmActivity extends AppCompatActivity implements SensorEventListener, StepListener {

    // The following are used for the shake detection
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mStepDetector;
    private SimpleStepDetector simpleStepDetector;

    private static final int THRESHOLD_STEPS = 20;
    private int accNumSteps;
    private int stepDetectorNumSteps;

    private TextView textViewAccSteps;
    private TextView textViewStepDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedometer_alarm);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR), SensorManager.SENSOR_DELAY_GAME);

        simpleStepDetector = new SimpleStepDetector();
        simpleStepDetector.registerListener(this);

        textViewAccSteps = findViewById(R.id.textViewAccSteps);
        textViewStepDetector = findViewById(R.id.textViewStepDetector);

        accNumSteps = 0;
        stepDetectorNumSteps = 0;

        textViewAccSteps.setText("Accelerometer: " + Integer.toString(accNumSteps));
        textViewStepDetector.setText("Step detector: " + Integer.toString(stepDetectorNumSteps));
    }

    public void onStop(View view) {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        accNumSteps = 0;
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    sensorEvent.timestamp, sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
        }
        if  (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            //Log.d("GSL", "update on step!");

            stepDetectorNumSteps++;
            textViewStepDetector.setText(new String("Step detector: ") + Integer.toString(stepDetectorNumSteps));
            if (stepDetectorNumSteps > THRESHOLD_STEPS)
                finish();
        }
    }

    @Override
    public void step() {
       accNumSteps++;
        textViewAccSteps.setText("Accelerometer: " + Integer.toString(accNumSteps));
        if (accNumSteps > THRESHOLD_STEPS)
            finish();
    }
}

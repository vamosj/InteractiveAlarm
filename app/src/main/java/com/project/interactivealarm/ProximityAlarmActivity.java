package com.project.interactivealarm;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ProximityAlarmActivity extends AppCompatActivity {
    private int times = 5;
    private boolean far = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proximity_alarm);


        SensorManager sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);

        final SensorEventListener proxSensorListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if( event.sensor.getType() == Sensor.TYPE_PROXIMITY)
                {
                    float proximity = event.values[0];
                    if (far && proximity < 1) {
                        far = false;
                        times--;
                    }

                    if(proximity >= 1) {
                        far = true;
                    }

                    if(times == 0)
                        finish();

                    ((TextView)findViewById(R.id.textViewTaps)).setText(String.valueOf(times));
                }
                Log.i("prox sensor", String.valueOf(event.values[0]));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        Sensor proxSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        if (null != proxSensor)
        {
            sensorManager.registerListener (
                    proxSensorListener,
                    proxSensor,
                    SensorManager.SENSOR_DELAY_NORMAL
            );
        }
        else
        {
            // messaggebox
        }
    }

    public void onStop(View view) {
        finish();
    }
}

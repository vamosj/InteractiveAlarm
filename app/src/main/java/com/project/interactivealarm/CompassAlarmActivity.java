package com.project.interactivealarm;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class CompassAlarmActivity extends AppCompatActivity implements SensorEventListener {
    private ImageView textViewImage;
    private TextView textViewDegrees;
    private SensorManager mSensorManager;

    private float goalDegree;
    private float currentDegree = 0f;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass_alarm);

        setGoal();

        textViewImage = findViewById(R.id.imageViewCompass);
        textViewDegrees = findViewById(R.id.textViewDegrees);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    private void setGoal() {
        TextView textViewDestination = findViewById(R.id.textViewDestination);
        int destinationId = ((int)(Math.random()*100)) % 8;

        switch (destinationId) {
            case 0:
                textViewDestination.setText("Move North: 0 degree");
                goalDegree = 0;
                break;
            case 1:
                textViewDestination.setText("Move Northeast: 45 degree");
                goalDegree = 45;
                break;
            case 2:
                textViewDestination.setText("Move East: 90 degree");
                goalDegree = 90;
                break;
            case 3:
                textViewDestination.setText("Move Southeast: 135 degree");
                goalDegree = 135;
                break;
            case 4:
                textViewDestination.setText("Move South: 180 degree");
                goalDegree = 180;
                break;
            case 5:
                textViewDestination.setText("Move Southwest: 225 degree");
                goalDegree = 225;
                break;
            case 6:
                textViewDestination.setText("Move West: 270 degree");
                goalDegree = 270;
                break;
            case 7:
                textViewDestination.setText("Move Northwest: 315 degree");
                goalDegree = 315;
                break;
            default:
                break;
        }
    }

    public void onStop(View view) {
        finish();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // get the angle around the z-axis rotated
        float degree = Math.round(event.values[0]);

        textViewDegrees.setText("Heading: " + Float.toString(degree) + " degrees");

        // create a rotation animation (reverse turn degree degrees)
        RotateAnimation ra = new RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);

        // how long the animation will take place
        ra.setDuration(210);

        // set the animation after the end of the reservation status
        ra.setFillAfter(true);

        // Start the animation
        textViewImage.startAnimation(ra);
        currentDegree = -degree;

        //Log.i("Math.abs(goalDegree - degree)", String.valueOf(Math.abs(goalDegree - degree)));
        //Log.i("goalDegree", String.valueOf(goalDegree));
        //Log.i("degree", String.valueOf(degree));

        if (Math.abs(goalDegree - degree) < 10) {
            finish();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}

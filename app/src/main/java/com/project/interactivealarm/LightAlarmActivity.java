package com.project.interactivealarm;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class LightAlarmActivity extends AppCompatActivity {
    private float lux_trashhold = 100f;
    private float previousLux = 0;
    private boolean firsttime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light_alarm);

        ((TextView)findViewById(R.id.textView6)).setText("Get to " + String.valueOf(lux_trashhold) + " Lux");

        SensorManager sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);

        final SensorEventListener lightSensorListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if( event.sensor.getType() == Sensor.TYPE_LIGHT)
                {
                    float currentLux = event.values[0];
                    if (!firsttime && Math.abs(currentLux - previousLux) > lux_trashhold)
                        finish();

                    firsttime = false;
                    previousLux = currentLux;

                    ((TextView)findViewById(R.id.textViewLux)).setText(String.valueOf(currentLux));
                }
                Log.i("light sensor", String.valueOf(event.values[0]));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        if (null != lightSensor)
        {
            sensorManager.registerListener (
                    lightSensorListener,
                    lightSensor,
                    SensorManager.SENSOR_DELAY_NORMAL
            );
        }
        else
        {
            // messaggebox
        }
    }

    public void onStop(View view) {
        finish();
    }
}

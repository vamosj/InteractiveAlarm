package com.project.interactivealarm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private static final int START_ALARM_ACTIVITY_RESULT_CODE = 42;

    private Timer timer = null;
    Intent alarmIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        alarmIntent = new Intent(this, RingingService.class);
    }

    public void onSetAlarm(View view) throws ParseException {
        Button button = findViewById(R.id.buttonSetAlarm);
        TimePicker timePicker = findViewById(R.id.timePicker);

        if(timer != null) {
            timer.cancel();
            timer = null;
            button.setText("Set Alarm");
            timePicker.setEnabled(true);
            return;
        }


        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute() + 60*hour;
        int seconds = minute * 60;

        int currentSeconds = new Date().getHours()*60*60 + new Date().getMinutes()*60 + new Date().getSeconds();

        if(currentSeconds > seconds)
            seconds += 24*60*60;

        long time = (seconds - currentSeconds) * 1000;

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                doAlarm();
            }
        }, time);

        button.setText("Cancel Alarm");
        timePicker.setEnabled(false);
    }

    public void onInstantAlarm(View view) {
        doAlarm();
    }

    private void doAlarm() {
        RadioButton radioPedometer = findViewById(R.id.radioButtonPedometer);
        RadioButton radioLight = findViewById(R.id.radioButtonLightSensor);
        RadioButton radioCompass = findViewById(R.id.radioButtonCompass);
        RadioButton radioShake = findViewById(R.id.radioButtonShake);
        RadioButton radioProx = findViewById(R.id.radioButtonProximity);
        RadioButton radioCamera = findViewById(R.id.radioButtonCamera);

        Intent intent;
        if(radioPedometer.isChecked()) {
            intent = new Intent(this, PedometerAlarmActivity.class);
        } else if(radioLight.isChecked()) {
            intent = new Intent(this, LightAlarmActivity.class);
        } else if(radioCompass.isChecked()) {
            intent = new Intent(this, CompassAlarmActivity.class);
        } else if(radioShake.isChecked()){
            intent = new Intent(this, ShakeAlarmActivity.class);
        } else if(radioProx.isChecked()) {
            intent = new Intent(this, ProximityAlarmActivity.class);
        }else if (radioCamera.isChecked()) {
            intent = new Intent(this, CameraAlarmActivity.class);
        } else {
            return;
        }

        startService(alarmIntent);
        startActivityForResult(intent, START_ALARM_ACTIVITY_RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == START_ALARM_ACTIVITY_RESULT_CODE) {
            stopService(alarmIntent);

            if(timer != null) {
                Button button = findViewById(R.id.buttonSetAlarm);
                TimePicker timePicker = findViewById(R.id.timePicker);
                timePicker.setEnabled(true);
                timer.cancel();
                timer = null;
                button.setText("Set Alarm");
            }
        }
    }
}

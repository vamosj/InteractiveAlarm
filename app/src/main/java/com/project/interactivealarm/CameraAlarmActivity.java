package com.project.interactivealarm;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.FaceDetector;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by uuty on 12/15/2017.
 */

public class CameraAlarmActivity extends AppCompatActivity {

    private final int REQUEST_IMAGE_CAPTURE = 1;
    private ImageView mImageView;
    private String mCurrentPhotoPath = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera_alarm);

        mImageView = (ImageView) findViewById(R.id.imageView1);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);

        try {
            dispatchTakePictureIntent();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void dispatchTakePictureIntent() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                File outputFile = createImageFile();
                mCurrentPhotoPath = outputFile.getPath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outputFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted and now can proceed
                    //mymethod(); //a sample method called

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    finish();
                }
                return;
            }
            // add other cases for more permissions
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // todo use appropriate resultCode in your case
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = null;

            if (mCurrentPhotoPath != null) {
                BitmapFactory.Options bfOptions = new BitmapFactory.Options();
                bfOptions.inPreferredConfig = Bitmap.Config.RGB_565;
                bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bfOptions);
            }

            mImageView.setImageBitmap(bitmap);
            mImageView.setVisibility(ImageView.VISIBLE);
            ((TextView)findViewById(R.id.textViewFaceNo)).setText("calculating...");

            new FindFacesTask().execute(bitmap);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onStop(View view) {
        finish();
    }

    private class FindFacesTask extends AsyncTask<Bitmap, Void, Integer> {
        @Override
        protected Integer doInBackground(Bitmap... bitmaps) {
            Bitmap bitmap = bitmaps[0];
            int NUMBER_OF_FACES = 1;
            FaceDetector.Face myFace[];
            FaceDetector myFaceDetect;
            int width, height;

            width = bitmap.getWidth();
            height = bitmap.getHeight();

            myFace = new FaceDetector.Face[NUMBER_OF_FACES];//acha ateh 4 faces numa imagem
            myFaceDetect = new FaceDetector(width, height, NUMBER_OF_FACES);

            int n = myFaceDetect.findFaces(bitmap, myFace);

            Log.i("width ", String.valueOf(width));
            Log.i("height ", String.valueOf(height));
            Log.i("find ", String.valueOf(n) + " face");

            return n;
        }

        @Override
        protected void onPostExecute(Integer result) {
            ((TextView) findViewById(R.id.textViewFaceNo)).setText(result.toString());

            Handler handler = new Handler();

            if (result > 0)
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 3000);
            else
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dispatchTakePictureIntent();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }, 3000);
        }
    }
}
